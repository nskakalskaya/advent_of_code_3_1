#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <algorithm>
#include <cmath>
#include <map>


using CoordsPair = std::pair<int, int>;

typedef void (*Function)(CoordsPair& previous_coordinate, std::vector<CoordsPair>& went_through, std::string& comma_coord);

void ProcessRightDirection(CoordsPair& previous_coordinate, std::vector<CoordsPair>& went_through, std::string& comma_coord)
{
    for (auto idx{previous_coordinate.first}; idx <= std::stoi(comma_coord.substr(1, comma_coord.size())) + previous_coordinate.first; ++idx)
    {
        went_through.push_back(std::make_pair(idx, previous_coordinate.second));
    }
    comma_coord.erase(comma_coord.begin());
    previous_coordinate.first += std::stoi(comma_coord);
}

void ProcessLeftDirection(CoordsPair& previous_coordinate, std::vector<CoordsPair>& went_through, std::string& comma_coord)
{
    for (auto idx{previous_coordinate.first}; idx >= previous_coordinate.first - std::stoi(comma_coord.substr(1, comma_coord.size())); --idx)
    {
        went_through.push_back(std::make_pair(idx, previous_coordinate.second));
    }
    comma_coord.erase(comma_coord.begin());
    previous_coordinate.first -= std::stoi(comma_coord);
}

void ProcessUpDirection(CoordsPair& previous_coordinate, std::vector<CoordsPair>& went_through, std::string& comma_coord)
{
    for (auto idx{previous_coordinate.second}; idx <= std::stoi(comma_coord.substr(1, comma_coord.size())) + previous_coordinate.second; ++idx)
    {
        went_through.push_back(std::make_pair(previous_coordinate.first, idx));
    }
    comma_coord.erase(comma_coord.begin());
    previous_coordinate.second += std::stoi(comma_coord);
}

void ProcessDownDirection(CoordsPair& previous_coordinate, std::vector<CoordsPair>& went_through, std::string& comma_coord)
{
    for (auto idx{previous_coordinate.second}; idx >= (previous_coordinate.second - std::stoi(comma_coord.substr(1, comma_coord.size()))); --idx)
    {
        went_through.push_back(std::make_pair(previous_coordinate.first, idx));
    }
    comma_coord.erase(comma_coord.begin());
    previous_coordinate.second -= std::stoi(comma_coord);
}

void ProcessLineToPoints(const std::string& line, std::vector<CoordsPair>& wire_path)
{
    /*
     * Define an std::map to map given direction to functions with required operations to perform
     */
    using DirectionsMapping = std::map<char, Function>;
    DirectionsMapping directions_map;
    directions_map.emplace('R', &ProcessRightDirection);
    directions_map.emplace('L', &ProcessLeftDirection);
    directions_map.emplace('U', &ProcessUpDirection);
    directions_map.emplace('D', &ProcessDownDirection);

    /*
     * Set previous_coordinate to origin (0;0).
     * It will store the coordinate of the previous wire position
     * throughout the whole wire path -> it should be initialized to zero only once before cycle begins
     * (otherwise it will be set to zero every time and we don't need it)
     */
    CoordsPair previous_coordinate{0,0};
    for (std::string::const_iterator it{line.begin()}; it!=line.end(); ++it)
    {
        /* Read the pair 'Direction, number of steps', e.g. R8 - go right, 8 steps
         *
         */
        std::string path_command_line{it, std::find(it, line.end(), ',')};
        if (path_command_line.empty())
        {
            throw std::runtime_error("Error while parsing commands list");
        }
        /*
         * Find the needed direction literal in the map
         */
        const auto iter{directions_map.find(path_command_line.at(0))};
        if (iter != directions_map.end())
        {
            /*
             * Process the operations for this direction
             */
            (*iter->second)(previous_coordinate, wire_path, path_command_line);
        }
        else
        {
            throw std::runtime_error("Error while parsing commands list");
        }

        if (it!= line.end() - path_command_line.size() - 1)
        {
            it += path_command_line.size() + 1;
        }
        else
        {
            break;
        }
    }
}

std::uint32_t CompareCalculatedPoints(std::vector<CoordsPair>& first_line_vector, std::vector<CoordsPair>& second_line_vector)
{
    std::uint32_t result{std::numeric_limits<std::uint32_t>::max()};
    /*
     * Sort vector with path coordinates
     * to easily find the common points
     */
    std::sort(first_line_vector.begin(), first_line_vector.end());
    std::sort(second_line_vector.begin(), second_line_vector.end());
    std::vector<CoordsPair> common_points{};
    static_cast<void>(set_intersection(first_line_vector.begin(), first_line_vector.end(), second_line_vector.begin(), second_line_vector.end(),
                     std::back_inserter(common_points)));
    /*
     * common_points now has some std::pair of points that were
     * same for both first and second wire
     */
    for (const auto& point: common_points)
    {
        /*
         * (0;0) coordinate does not matter since
         * it's origin for both wires
         */
        if (point.first == 0 && point.second == 0)
            continue;
        /*
         * Calculate Manhattan distance
         */
        const auto manhattan_distance = []( const CoordsPair& coords_pair ) -> std::uint32_t
        {
            return std::abs(coords_pair.first) + std::abs(coords_pair.second);
        };
        std::cout << "(" << point.first << " ; " << point.second << ")" << " distance " << manhattan_distance(point) << std::endl;
        /*
         * Change result only if the current value is less than the previous one
         * since we're looking for the shortest path from the origin
         */
        result = (result >= manhattan_distance(point)) ? manhattan_distance(point) : result;
    }
    return result;
}


int main()
{
    std::string inputFilename{"/home/qxz0abk/advent_of_code/advent_of_code_3_1/input_3_1.txt"};
    try {
        std::ifstream inputStream(inputFilename);
        std::string first_line{};
        std::string second_line{};
        /* https://adventofcode.com/2019/day/3
         *
         * Read two lines of commands
         * First line contains commands that describe the path of the first wire
         * Second line contains commands that describe the path of the second wire
         *
         * The goal is to find the coordinate of the intersection point of these wires and
         * calculate the Manhattan distance for this point.
         * It is possible that wires have more than one point of intersection, so the desired
         * point ought to be the closest one to the origin.
         *
         */
        std::getline(inputStream, first_line, '\n');
        std::getline(inputStream, second_line, '\n');
        if (first_line.empty() || second_line.empty())
        {
            std::cout << "One or both strings are empty, exiting." << std::endl;
            return -1;
        }

        std::vector<CoordsPair>first_wire_path{};
        std::vector<CoordsPair>second_wire_path{};
        /*
         * Calculate the path for each wire
         * first_wire_path and second_wire_path contain all coordinates the wire passed through
         * For example, if wire went 3 to the right from the origin (command R3),
         * then first_wire_path contains (0;0), (0;1), (0;2) and (0;3).
         */
        ProcessLineToPoints(first_line, first_wire_path);
        ProcessLineToPoints(second_line, second_wire_path);
        /*
         * Find common points in both wire paths and
         * calculate Manhattan distance for each pair
         * in order to find the least one
         */
        std::cout << CompareCalculatedPoints(first_wire_path, second_wire_path) << std::endl;
    }
    catch (const std::ifstream::failure &e) {
        std::cout << "Caught exception " << e.what() << std::endl;
        return -1;
    }
    catch (const std::runtime_error &e)
    {
        std::cout << "Caught exception " << e.what() << std::endl;
        return -1;
    }
    return 0;
}

